﻿using UnityEngine;
using System.Collections;

public class FireBall : Ball
{
    [SerializeField] float maximumLifetime = 3f;
    [SerializeField] GameObject fireEffectVisual;
    [SerializeField] AudioClip wallHittingSound;
    AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        // subscribe to events we want to know about
        ballInteraction.OnBallCollision += BallInteraction_OnBallCollision;
        ballInteraction.OnBallLaunch += BallInteraction_OnBallLaunch;
    }

    private void BallInteraction_OnBallLaunch(object sender, System.EventArgs e)
    {
        Destroy(gameObject, maximumLifetime);
    }

    private void BallInteraction_OnBallCollision(object sender, Ball_Interaction.CollisionEventArgs e)
    {
        if (e.collision.gameObject.tag == "Enemy" && e.collision.gameObject.GetComponent<OnFire>() == null) // if not already on Fire
        {
            OnFire fireScript = e.collision.gameObject.AddComponent<OnFire>(); // set it on fire
            fireScript.StartFire(fireEffectVisual);
        }
        else if (e.collision.gameObject.tag == "Wall")
        {
            audioSource.clip = wallHittingSound;
            audioSource.Play();
        }
    }
}
