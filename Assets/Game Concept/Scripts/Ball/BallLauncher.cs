﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BallLauncher : MonoBehaviour {

    protected bool holdingBall;
    [SerializeField] protected Transform ballStartPos;
    protected GameObject currentBall;
    public float platformRadius;
    public event EventHandler OnBallPull;
    public event EventHandler OnBallRelease;

    // when player starts interaction with ball
    protected void RaiseBallPull()
    {
        var handler = OnBallPull;
        if (handler != null)
        {
            handler(this, EventArgs.Empty);
        }
    }

    // when player stops interacting with ball
    protected void RaiseBallRelease()
    {
        var handler = OnBallRelease;
        if (handler != null)
        {
            handler(this, EventArgs.Empty);
        }
    }
}
