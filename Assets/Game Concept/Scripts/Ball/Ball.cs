﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Ball_Interaction))]
public class Ball : MonoBehaviour {
    [SerializeField] protected Ball_Interaction ballInteraction;
}
