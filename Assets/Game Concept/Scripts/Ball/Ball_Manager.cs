﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class Ball_Manager : MonoBehaviour {
	bool ballAvailable;
    bool spawningEnabled = true;
    public Dictionary<GameObject, SpawnTimer> ballSpawners = new Dictionary<GameObject, SpawnTimer>(); // spawn timers to match to each prefab - can't have a timer on the ball because it isn't instantiated!
    public List<GameObject> allAvailableBalls;
	public GameObject selectedBallType; // ball prefab player has selected in UI
    public GameObject unreleasedBall; // ball that is physically sitting on the platform
	public Transform startPos; // where the ball start - centre of the platform

    public event EventHandler OnBallSpawn;

    // Use this for initialization
    void Start () {
        foreach(GameObject aBall in allAvailableBalls)
        {
            SpawnTimer newTimer = new SpawnTimer(aBall.GetComponent<Ball_Interaction>().fireRate, aBall.name);
            ballSpawners.Add(aBall, newTimer); // create spawn timers for all the balls
        }
        selectedBallType = allAvailableBalls[0]; // default to top of the list

        GameObject.FindObjectOfType<GameManager>().OnGameOver += Ball_Manager_OnGameOver;
    }

    private void Ball_Manager_OnGameOver(object sender, GameManager.GameEventArgs e)
    {
        if (e.playerWon == false) // if the player lost
        {
            DisableBallSpawning(); // stop them firing balls
        }
    }

    // Update is called once per frame
    void Update () {
        if (spawningEnabled)
        {
            // update all of the spawn timers
            foreach (SpawnTimer aSpawner in ballSpawners.Values)
            {
                aSpawner.UpdateSpawner(Time.deltaTime);
            }
            // if there is no ball on the platform and current ball selection has completed its cooldown
            if (!ballAvailable && CheckBallSpawnTimer(selectedBallType))
            {
                SpawnBall(selectedBallType);
            }
        }
	}

    public void DisableBallSpawning()
    {
        spawningEnabled = false;
        if (unreleasedBall != null) Destroy(unreleasedBall);
        ballAvailable = false;
    }

    public void EnableBallSpawning()
    {
        spawningEnabled = false;
    }

    private void RaiseBallSpawn()
    {
        var handler = OnBallSpawn;
        if (handler != null)
        {
            handler(this, EventArgs.Empty);
        }
    }

    public void SpawnBall(GameObject ballToSpawn)
    {
        if (unreleasedBall != null) Destroy(unreleasedBall);
        unreleasedBall = Instantiate(ballToSpawn, startPos.position, Quaternion.identity); // respawn ball
        ballAvailable = true;
        RaiseBallSpawn();
    }

    bool CheckBallSpawnTimer(GameObject ballToCheck)
    {
        SpawnTimer ballTimer;
        ballSpawners.TryGetValue(selectedBallType, out ballTimer); // get the released balls timer
        return ballTimer.isSpawnable;
    }

    public void ChangeBall(GameObject newBallPrefab)
    {
        if (unreleasedBall != null) Destroy(unreleasedBall); // destroy current ball if it exists
        ballAvailable = false;
        selectedBallType = newBallPrefab; // set to new ball
    }

    // Set ball manager to state of just realeasing the ball, start the respawn counter
    public void ReleaseBall()
    {
        ballAvailable = false;
        SpawnTimer ballTimer;
        ballSpawners.TryGetValue(selectedBallType, out ballTimer); // get the released balls timer
        ballTimer.StartCooldown();
        unreleasedBall = null;
    }
}
