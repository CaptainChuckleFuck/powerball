﻿using System;

[Serializable]
public class SpawnTimer {
    public string name; // the timers identifier
    public bool isSpawnable; // can the object be spawned currently - only public to show in Unity editor
    float spawnCountdown = 0; // internal timer
    float cooldown;

    public SpawnTimer(float spawnCooldown, string timerName)
    {
        cooldown = spawnCooldown;
        name = timerName;
        spawnCountdown = 0;
    }

    public void UpdateSpawner(float deltaTime)
    {
        if(!isSpawnable) // while ball is on cooldown after firing
        {
            spawnCountdown -= deltaTime; // tick down counter until it can fire again
            if(spawnCountdown < 0)
            {
                isSpawnable = true;
            }
        }
    }

    public void StartCooldown() // when the ball is fired from the platform, start cooldown
    {
        isSpawnable = false;
        spawnCountdown = cooldown;
    }
}
