﻿using UnityEngine;
using System.Collections;

public class EnergyBall : Ball
{
    [SerializeField] AudioClip wallCollisionSound;
    [SerializeField] AudioClip launchingSound;
    [SerializeField] AudioSource audioSource;
    [SerializeField] GameObject popEffect;
    [SerializeField] float popEffectLiftime = 1f;
    [SerializeField] float maximumLifetime = 3f;

    private void Start()
    {
        // subscribe to events we want to know about
        ballInteraction.OnBallCollision += BallInteraction_OnBallCollision;
        ballInteraction.OnBallLaunch += BallInteraction_OnBallLaunch;
    }

    private void BallInteraction_OnBallLaunch(object sender, System.EventArgs e)
    {
        audioSource.clip = launchingSound;
        audioSource.Play();
        Destroy(gameObject, maximumLifetime);
    }

    private void BallInteraction_OnBallCollision(object sender, Ball_Interaction.CollisionEventArgs e)
    {
        if (e.collision.gameObject.tag == "Wall") // if we hit a wall
        {
            audioSource.clip = wallCollisionSound; // play a wall hit sound
            audioSource.Play();
        }
        else if (e.collision.gameObject.tag == "Enemy")
        {
            ReleasePopEffect();
        }
    }

    private void ReleasePopEffect()
    {
        popEffect.SetActive(true); // turn on the pop effect
        popEffect.transform.parent = null; // release it as its own object
        Destroy(popEffect, popEffectLiftime); // set a destroy on it after its completed
    }
}
