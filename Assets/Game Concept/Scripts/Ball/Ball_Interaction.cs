﻿using UnityEngine;
using System.Collections;
using System;

public class Ball_Interaction : MonoBehaviour
{
    public GameObject collisionEffect; // should be self contained object, its simply deparented from the ball and should handle its own destruction
    public int ballDamage;
    public DamageTypes baseDamageType;
    public float fireRate; // how long does it take for this ball to reform
    public float speedMultiplier;
    private bool ballLaunched = false;

    public event EventHandler<CollisionEventArgs> OnBallCollision;
    public event EventHandler OnBallLaunch;

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Enemy") // when you hit an enemy the ball should destroy
        {
            GameObject tempObj = (GameObject)Instantiate(collisionEffect, gameObject.transform.position, Quaternion.identity); // when you hit enemy, spawn collision effect
            RaiseBallCollision(coll);
            Destroy(gameObject);
        }

        RaiseBallCollision(coll);
    }

    public void RaiseBallLaunched()
    {
        if (!ballLaunched) // ensure single shot event is not called twice
        {
            ballLaunched = true;
            Debug.Log("Event : OnBallLaunched : Ball_Interaction : " + gameObject.name);
            var handler = OnBallLaunch;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }
    }

    private void RaiseBallCollision(Collision2D collision)
    {
        var handler = OnBallCollision;
        if (handler != null)
        {
            handler(this, new CollisionEventArgs(collision));
        }
    }

    public class CollisionEventArgs : EventArgs
    {
        public Collision2D collision { get; private set; }
        public CollisionEventArgs(Collision2D collision)
        {
            this.collision = collision;
        }
    }
}