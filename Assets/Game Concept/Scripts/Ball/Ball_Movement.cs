﻿using UnityEngine;
using System.Collections;

public class Ball_Movement : MonoBehaviour {
	bool holdingBall;
	bool ballFired;
	Vector3 startPos;
	public float platformRadius;
	public float baseSpeed;
	public float respawnTime;
	public float lifeTime;
	// Use this for initialization
	void Start () {
		ballFired = false;
		holdingBall = false;
		startPos = transform.position;
	}

    // Update is called once per frame
    void Update()
    {
        if (!ballFired)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                if (GetComponent<CircleCollider2D>().OverlapPoint(mousePos))
                    holdingBall = true;
            }
            if (Input.GetMouseButtonUp(0))
            {
                holdingBall = false;
                ReleaseBall();
            }

            if (holdingBall)
                FollowMouse();
        }
        else
        {
            if (lifeTime <= 0f)
            { // lifetime timer countsdown once fired
                Destroy(gameObject); // hits zero destroy
            }
            else
            {
                lifeTime -= Time.deltaTime; // decrement timer
            }
        }
    }

    //TODO: sort out my magic numbers used here
    void FollowMouse (){
		Vector3 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		mousePos.z = 0.36f;
		Vector3 distanceFromStart = mousePos - startPos;
		//print(distanceFromStart.sqrMagnitude);
		if(distanceFromStart.sqrMagnitude < 5.0f) {
			transform.position = mousePos;
		}
		else {
			distanceFromStart = distanceFromStart.normalized * platformRadius;
			distanceFromStart.z = 0.36f;
			distanceFromStart.x -= 6.7f;
			transform.position = distanceFromStart;
		}
	}

	void ReleaseBall () {
		if (transform.position != startPos) {
			ballFired = true;
			Vector3 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			mousePos.z = 0.36f;
			Vector3 vectToStart = startPos - mousePos;
			GetComponent<Rigidbody2D>().AddForce(new Vector2(vectToStart.x * baseSpeed * (GetComponent<Rigidbody2D>().mass/2f), vectToStart.y * baseSpeed * (GetComponent<Rigidbody2D>().mass/2f)));
			GameObject.FindGameObjectWithTag ("Platform").GetComponent<Ball_Manager>().ReleaseBall();
		}
	}
}
