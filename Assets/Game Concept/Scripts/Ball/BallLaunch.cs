﻿using UnityEngine;
using System.Collections;
using System;

public class BallLaunch : BallLauncher
{
    AudioSource audioSource;
    public AudioClip pullingSound;
    [SerializeField] float minPitch = 0.8f;
    [SerializeField] float maxPitch = 1.5f;
    [SerializeField] Ball_Manager ballManager;
    // Use this for initialization
    void Start()
    {
        ballManager.OnBallSpawn += BallManager_OnBallSpawn;
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = pullingSound;
        if(ballManager.unreleasedBall != null) // ensure we get a reference to the intial ball
            FreshBallInit(ballManager.unreleasedBall);
    }

    private void BallManager_OnBallSpawn(object sender, EventArgs e)
    {
        FreshBallInit(ballManager.unreleasedBall);
    }

    void FreshBallInit(GameObject newBall)
    {
        // set values to default for a new ball 
        StopPullSound();
        holdingBall = false;
        // disable physics to stop ships hitting it before release
        Rigidbody2D ballRigidbody = newBall.GetComponent<Rigidbody2D>(); 
        ballRigidbody.simulated = false;
        currentBall = newBall;
    }

    

    void StartPullSound()
    {
        audioSource.pitch = 1f;
        audioSource.loop = true;
        audioSource.Play();
    }

    void StopPullSound()
    {
        audioSource.pitch = 1f;
        audioSource.loop = false;
        audioSource.Stop();
    }

    /// <summary>
    /// Alters pitch to a point between max and min Pitch calculated relative to the balls distance from the centre
    /// </summary>
    /// <param name="distanceFromCentre"></param>
    void AlterPullPitch(float distanceFromCentre)
    {
        float pitchPoint = distanceFromCentre / platformRadius; // absolute pitch point scaled between 0 - 1;
        // work out scale multiplier to match different between min and max pitch to 0 - 1
        float scaleMultiplier = (maxPitch - minPitch);
        float relativePitchPoint = scaleMultiplier * pitchPoint + minPitch; // get the new pitch point
        audioSource.pitch = relativePitchPoint;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentBall != null)
        {
            UpdateBallInteraction();
        }
    }

    void UpdateBallInteraction()
    {
        // if mouse is pressed within ball collider
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (currentBall.GetComponent<CircleCollider2D>().OverlapPoint(mousePos))
            {
                holdingBall = true;
                StartPullSound();
                RaiseBallPull();
            }
        }
        // when mouse is released
        if (holdingBall && Input.GetMouseButtonUp(0))
        {
            holdingBall = false;
            ReleaseBall();
        }

        if (holdingBall)
        {
            float distance = FollowMouse();
            AlterPullPitch(distance);
        }
    }

    //TODO: sort out my magic numbers used here
    float FollowMouse()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0.36f;
        Vector3 distanceFromStart = mousePos - ballStartPos.position;
        //print(distanceFromStart.sqrMagnitude);
        if (distanceFromStart.sqrMagnitude < 5.0f)
        {
            currentBall.transform.position = mousePos;
        }
        else
        {
            distanceFromStart = distanceFromStart.normalized * platformRadius;
            distanceFromStart.z = 0.36f;
            distanceFromStart.x -= 6.7f;
            currentBall.transform.position = distanceFromStart;
        }

        return Mathf.Clamp(distanceFromStart.magnitude,0,platformRadius);
    }

    void ReleaseBall()
    {
        RaiseBallRelease(); // player has stopped interacting with ball
        StopPullSound();
        if (currentBall.transform.position != ballStartPos.position)
        {           
            AddLaunchForce();
            // TODO: Use an event that the Ball_Manager subscribes to rather than this quick call
            GameObject.FindGameObjectWithTag("Platform").GetComponent<Ball_Manager>().ReleaseBall(); // tell the ball manager a ball has been launched
            currentBall.GetComponent<Ball_Interaction>().enabled = true; // turn on ball interaction with the enviroment            
            currentBall.GetComponent<Ball_Interaction>().RaiseBallLaunched(); // tell the ball it has been launched
        }
    }

    void AddLaunchForce()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 directionToStart = (ballStartPos.position - mousePos);
        // if player pulled back passed the edge of the platform edge, scale vector to its maximum (from platform edge to platform centre)
        if (directionToStart.magnitude > platformRadius)
        {
            directionToStart = directionToStart.normalized * platformRadius;
        }
        Rigidbody2D ballRigidbody = currentBall.GetComponent<Rigidbody2D>();
        ballRigidbody.simulated = true; // make it collidable!
        float forceMultiplier = currentBall.GetComponent<Ball_Interaction>().speedMultiplier * ballRigidbody.mass;
        ballRigidbody.AddForce(directionToStart * forceMultiplier);
    }
}
