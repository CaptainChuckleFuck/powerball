﻿using System;
using UnityEngine;

public class ActionSceneManager : GameManager {
    public WorldSpaceUI worldSpaceUI;
    public ScreenSpaceUI screenSpaceUI;
    public Vehicle_Manager vehicleManager;
    private UI_Manager UIManager;
    private Player player;
    [SerializeField] private PlayerStats playerStartingStats;
    public int requiredSynchScore = 100;
    public int startingSynchScore = 0;
    int currentSynchScore;

    // Use this for initialization
    void Start () {
        currentSynchScore = startingSynchScore;
        player = new Player(new PlayerStats(playerStartingStats.maxHealth)); // create a new player for the scene
        InitaliseUI();
        InitialiseVehicles();
        player.OnPlayerDeath += Player_OnPlayerDeath;
    }

    private void Player_OnPlayerDeath(object sender, EventArgs e)
    {
        RaiseGameOver(false);
    }

    // When a new vehicle is spawned, subscript to its death event
    private void VehicleManager_OnVehicleSpawn(object sender, VehicleEventArgs e)
    {
        e.TheVehicle.OnVehicleDeath += ActionSceneManager_OnVehicleDeath;
    }

    // Whenver a vehicle dies, add its score value to the score counter
    private void ActionSceneManager_OnVehicleDeath(object sender, VehicleEventArgs e)
    {
        currentSynchScore += e.TheVehicle.scoreValue; // increment score counter
        UIManager.screenUI.SetEnergySynchVisual(currentSynchScore);
        if (currentSynchScore >= requiredSynchScore) // if player earns enough score, they win!
            RaiseSynchroniserCharged(); // called to end the scenario, the player has won
    }    

    void InitaliseUI()
    {
        UIManager = new UI_Manager(worldSpaceUI, screenSpaceUI, player); // attach ingame UI managers
        // set starting values for sliders
        UIManager.screenUI.InitHealthSlider(0, playerStartingStats.maxHealth, playerStartingStats.maxHealth);
        UIManager.screenUI.InitEnergySlider(0, requiredSynchScore, currentSynchScore);
    }

    private void ActionSceneManager_OnGameCompletion(object sender, GameEventArgs e)
    {
        throw new NotImplementedException();
    }


    // link to the vehicle manager and the vehicles to the UI manager
    void InitialiseVehicles()
    {
        Vehicle.UiManager = UIManager; // let vehicle class know what UI it should send its UI requests to
        OnSynchroniserCharged += vehicleManager.VehicleManager_OnSynchroniserCharged; // let vehicle manager know where its going to find out about game completion from
        vehicleManager.OnVehicleSpawn += VehicleManager_OnVehicleSpawn;
        vehicleManager.OnAllVehiclesDestroyed += VehicleManager_OnAllVehiclesDestroyed;
    }

    private void VehicleManager_OnAllVehiclesDestroyed(object sender, EventArgs e)
    {
        RaiseGameOver(true);
    }
    public Player GetPlayer()
    {
        return player;
    }
}