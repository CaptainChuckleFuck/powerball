﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameManager : MonoBehaviour {
    bool gameCompleted = false;
    bool synchCharged = false;

    public event EventHandler OnSynchroniserCharged;
    public event EventHandler<GameEventArgs> OnGameOver;

    public class GameEventArgs : EventArgs
    {
        public bool playerWon { get; private set; }

        public GameEventArgs(bool didPlayerWin)
        {
            playerWon = didPlayerWin;
        }
    }

    // Raise an event signalling that the player has built up to full power
    protected void RaiseSynchroniserCharged()
    {
        if (!synchCharged) // ensure single shot event is not called twice
        {
            synchCharged = true;
            Debug.Log("Event : OnSynchroniserCharged : " + this);
            var handler = OnSynchroniserCharged;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }
    }

    // Raise an event signalling completion of the scenario and pass whether the player won or not
    protected void RaiseGameOver(bool didPlayerWin)
    {
        if (!gameCompleted) // ensure single shot event is not called twice
        {
            gameCompleted = true;
            Debug.Log("Event : OnGameCompletion : " + this);
            var handler = OnGameOver;
            if (handler != null)
            {
                handler(this, new GameEventArgs(didPlayerWin));
            }
        }
    }
}
