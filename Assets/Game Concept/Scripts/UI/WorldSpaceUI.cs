﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WorldSpaceUI : MonoBehaviour
{
    public GameObject hitTextPrefab;

    public void SpawnHitMarker(int damageAmount, float xPos, float yPos, float zPos)
    {
        GameObject newText = (GameObject)Instantiate(hitTextPrefab);
        newText.GetComponent<Text>().text = damageAmount.ToString();
        newText.transform.SetParent(gameObject.transform);
        newText.transform.localPosition = new Vector3(xPos, yPos, newText.transform.position.z);
    }
}
