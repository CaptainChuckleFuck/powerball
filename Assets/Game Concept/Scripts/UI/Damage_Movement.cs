﻿using UnityEngine;
using System.Collections;

public class Damage_Movement : MonoBehaviour {
	public float floatSpeed;
	public float lifeTime;
	// Use this for initialization
	void Start () {
		Destroy (gameObject,lifeTime);
	}
	
	// Update is called once per frame
	void Update () {
		transform.localPosition += Vector3.up*Time.deltaTime*floatSpeed;
	}
}
