﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScreenSpaceUI : MonoBehaviour
{
    [SerializeField] private Slider playerHealthSlider;
    [SerializeField] private Slider energySynchSlider;
    [SerializeField] private GameObject shakeText;
    [SerializeField] private GameObject winUI;
    [SerializeField] private GameObject loseUI;
    [SerializeField] private GameObject restartButton;

    public void Start()
    {
        ActionSceneManager actionSceneManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<ActionSceneManager>();
        actionSceneManager.OnGameOver += ScreenSpaceUI_OnGameOver;
        actionSceneManager.OnSynchroniserCharged += ScreenSpaceUI_OnSynchroniserCharged;
    }

    public void ChangeScene(string newScene)
    {
        SceneManager.LoadScene(newScene);
    }

    public void InitSlider(Slider aSlider, int minVal, int maxVal, int startVal)
    {
        aSlider.minValue = minVal;
        aSlider.maxValue = maxVal;
        aSlider.value = startVal;
    }

    public void InitHealthSlider(int minVal, int maxVal, int startVal)
    {
        InitSlider(playerHealthSlider, minVal, maxVal, startVal);
    }

    public void InitEnergySlider(int minVal, int maxVal, int startVal)
    {
        InitSlider(energySynchSlider, minVal, maxVal, startVal);
    }

    public void SetPlayerHealthVisual(int newHealth)
    {
        playerHealthSlider.value = newHealth;
    }

    public void SetEnergySynchVisual(int newSynchValue)
    {
        energySynchSlider.value = newSynchValue;
    }

    private void ScreenSpaceUI_OnSynchroniserCharged(object sender, EventArgs e)
    {
        shakeText.SetActive(true);
    }

    private void ScreenSpaceUI_OnGameOver(object sender, GameManager.GameEventArgs e)
    {
        // set UI to show relevant text and restart / replay button
        shakeText.SetActive(false);
        if (e.playerWon == true)
            winUI.SetActive(true);
        else
            loseUI.SetActive(true);
    }
}