﻿using System;
using UnityEngine;

[Serializable]
public class UI_Manager {
    public WorldSpaceUI worldUI;
    public ScreenSpaceUI screenUI;
    private Player player;

    public UI_Manager (WorldSpaceUI worldSpaceUI, ScreenSpaceUI screenSpaceUI, Player player)
    {
        worldUI = worldSpaceUI;
        screenUI = screenSpaceUI;
        this.player = player;
        player.OnPlayerDamaged += Player_OnPlayerDamaged; // subscribe to player events
    }

    private void Player_OnPlayerDamaged(object sender, EventArgs e)
    {
        SetPlayerHealthVisual(player.GetPlayerStats().CurrentHealth);
    }

    public void SpawnHitMarker (int damageAmount, float xPos, float yPos, float zPos)
    {
        worldUI.SpawnHitMarker(damageAmount, xPos, yPos, zPos);
	}

    public void SetPlayerHealthVisual(int newHealth)
    {
        screenUI.SetPlayerHealthVisual(newHealth);
    }
}