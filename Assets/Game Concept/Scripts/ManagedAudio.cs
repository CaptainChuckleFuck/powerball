﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagedAudio : MonoBehaviour {
    AudioSource managedAudioSource;
    float startingVolume = 1f;
    [SerializeField] AudioManager.SoundType audioType = AudioManager.SoundType.Effect; // by default assume its an effect (there is only 1 thing playing music at a time so its a good bet)
    [SerializeField] AudioManager audioManager;
    bool initialised = false;
	// Use this for initialization
	void Start () {
        Initalize();
    }

    public void Initalize()
    {
        if (initialised == false)
        {
            managedAudioSource = GetComponent<AudioSource>();
            startingVolume = managedAudioSource.volume;
            if (CheckAudioManagerExists())
            {
                audioManager.OnVolumeChange += AudioManager_VolumeUpdate; // subscribe to updates
                AudioManager_VolumeUpdate(this, audioManager.GetAudioArgs()); // get values from AudioManager
            }

            initialised = true;
        }
    }

    bool CheckAudioManagerExists()
    {
        if (audioManager == null)
        {
            audioManager = GameObject.FindObjectOfType<AudioManager>();
            if (audioManager == null)
            {
                Debug.LogError("No AudioManager assigned to this pre-instatiated object and none can be found in the scene\nThe ManagedAudio script never alter the audio of: " + gameObject.name);
                return false;
            }
        }
        return true;
    }

    public void AudioManager_VolumeUpdate(object sender, AudioManager.AudioEventArgs e)
    {
        switch (audioType) // figure out which value to update
        {
            case AudioManager.SoundType.Music:
                UpdateVolume(startingVolume * e.musicVolume);
                break;
            case AudioManager.SoundType.Effect:
                UpdateVolume(startingVolume * e.effectsVolume);
                break;
            default:
                break;
        }
    }

    void UpdateVolume(float newVolume)
    {
        managedAudioSource.volume = newVolume;
    }
}
