﻿using System;
[Serializable]
public class Player
{
    public event EventHandler OnPlayerDamaged;
    public event EventHandler OnPlayerDeath;
    
    PlayerStats playerStats;

    public Player(PlayerStats playerStats)
    {
        this.playerStats = playerStats;
    }

    /// <summary>
    /// Deliver damage to the players playerStats.health value. Triggers the OnPlayerDamaged Event.
    /// </summary>
    /// <param name="damageAmount"></param>
    public void TakeDamage(int damageAmount)
    {
        playerStats.SetPlayerHealth(playerStats.CurrentHealth - damageAmount);
        // raise the player damaged event
        EventHandler handler = OnPlayerDamaged;
        if (handler != null)
        {
            handler(this, null);
        }

        if (playerStats.CurrentHealth <= 0)
            PlayerDied();
    }

    void PlayerDied()
    {        
        // raise the player death event
        EventHandler handler = OnPlayerDeath;
        if (handler != null)
        {
            handler(this, null);
        }
    }

    public PlayerStats GetPlayerStats()
    {
        return playerStats;
    }
}
