﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShake : MonoBehaviour {
    [SerializeField] float shakeForceMultiplier = 1;
    [SerializeField] float randomisationMin = 1;
    [SerializeField] float randomisationMax = 1;
    bool isShaking = false;

    List<Rigidbody2D> allVehicles = new List<Rigidbody2D>();
	// Use this for initialization
	void Awake () {
        isShaking = false;
        GameManager sceneManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        sceneManager.OnSynchroniserCharged += PlayerShake_OnSynchroniserCharged;
        sceneManager.OnGameOver += PlayerShake_OnGameOver;
    }

    private void PlayerShake_OnGameOver(object sender, GameManager.GameEventArgs e)
    {
        StopShaking();
    }

    private void PlayerShake_OnSynchroniserCharged(object sender, System.EventArgs e)
    {
        StartShaking();
    }

    void StartShaking()
    {
        isShaking = true;
        GameObject[] enemyVehicles = GameObject.FindGameObjectsWithTag("Enemy"); // get all remaining enemies - no more should spawn at this point
        foreach (GameObject aVehicle in enemyVehicles)
        {
            allVehicles.Add(aVehicle.GetComponent<Rigidbody2D>());

            aVehicle.GetComponent<Vehicle>().OnVehicleDeath += PlayerShake_OnVehicleDeath; // for updating list if any are killed
            aVehicle.GetComponent<Vehicle_Movement>().enabled = false; // stop their movement 
        }
    }

    void StopShaking()
    {
        isShaking = false;
    }

    private void PlayerShake_OnVehicleDeath(object sender, VehicleEventArgs e)
    {
        allVehicles.Remove(e.TheVehicle.gameObject.GetComponent<Rigidbody2D>()); // vehicle is dead, stop applying forces to it
    }

    // Update is called once per frame
    void Update()
    {
        if (isShaking)
            ShakeVehicles();
    }

    void ShakeVehicles()
    {
        foreach (Rigidbody2D aVehicle in allVehicles)
        {
            if (aVehicle != null)
            {
                //Vector3 shakeForce = (RandomVector3(randomisationMin, randomisationMax) + Input.acceleration) * shakeForceMultiplier;
                Vector3 shakeForce = (RandomVector3(randomisationMin, randomisationMax) + Vector3.zero) * shakeForceMultiplier;
                aVehicle.AddForce(shakeForce);
            }
        }
    }

    /// <summary>
    /// Generate a random Vector3 with values in the given range
    /// </summary>
    /// <param name="minVal"></param>
    /// <param name="maxVal"></param>
    /// <returns></returns>
    Vector3 RandomVector3(float minVal, float maxVal)
    {
        float x = Random.Range(minVal, maxVal);
        float y = Random.Range(minVal, maxVal);
        float z = Random.Range(minVal, maxVal);

        return new Vector3(x, y, z);
    }
}
