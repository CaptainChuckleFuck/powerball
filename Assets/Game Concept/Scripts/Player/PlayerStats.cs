﻿using System;

[Serializable]
public class PlayerStats {
    public int CurrentHealth { get; private set; }
    public int maxHealth;

    public PlayerStats (int maximumHealth)
    {
        maxHealth = maximumHealth;
        CurrentHealth = maxHealth;
    }

    // Set the players currentHealth variable but clamps it between 0 and max health
    public void SetPlayerHealth(int newHealth)
    {
        CurrentHealth = newHealth;
        if (CurrentHealth > maxHealth) CurrentHealth = maxHealth;
        else if (CurrentHealth < 0) CurrentHealth = 0;
    }
}
