﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour {
    float musicVolume = 1;
    float effectsVolume = 1;
    public event EventHandler<AudioEventArgs> OnVolumeChange;
    public enum SoundType { Music, Effect };
    public List<AudioSource> unitySceneSources; // can't dynamically search the scene as it ignores inactive objects
	// Use this for initialization
	void Start () {        
        InitManagement();
        SyncFromPrefs();
    }

    void InitManagement()
    {
        foreach (AudioSource anAudioSource in unitySceneSources)
        {
            ManagedAudio managedAudio = anAudioSource.GetComponent<ManagedAudio>();
            if (managedAudio == null)
            {
                managedAudio = anAudioSource.gameObject.AddComponent<ManagedAudio>(); // add the component
            }
            // forcibly initialise the object, to ensure it is subbed to the OnVolumeChange event before we sync
            managedAudio.Initalize();
        }
    }

    private void RaiseVolumeChange()
    {
        var handler = OnVolumeChange;
        if (handler != null)
        {
            handler(this, new AudioEventArgs(musicVolume,effectsVolume));
        }
    }

    public AudioEventArgs GetAudioArgs()
    {
        return new AudioEventArgs(musicVolume, effectsVolume);
    }


    /// <summary>
    /// Gets Values from player prefs and loads them into the AudioManager
    /// </summary>
    public void SyncFromPrefs()
    {
        if(PlayerPrefs.HasKey("Music_Volume")) // check if key exists
        {
            musicVolume = PlayerPrefs.GetFloat("Music_Volume"); // update
        }

        if(PlayerPrefs.HasKey("Effects_Volume")) // check if key exists
        {
            effectsVolume = PlayerPrefs.GetFloat("Effects_Volume"); // update value from prefs
        }
        RaiseVolumeChange(); // raise a value changed event to update all managed objects with the new settings
    }

    /// <summary>
    /// Saves current values in this class to prefs
    /// </summary>
    public void SyncToPrefs()
    {
        PlayerPrefs.SetFloat("Music_Volume", musicVolume);
        PlayerPrefs.SetFloat("Effects_Volume", effectsVolume);
        PlayerPrefs.Save();
    }

    public void SetMusicVolume(float newValue)
    {
        musicVolume = Mathf.Clamp(newValue, 0, 1);
        RaiseVolumeChange();
    }

    public void SetEffectsVolume(float newValue)
    {
        effectsVolume = Mathf.Clamp(newValue, 0, 1);
        RaiseVolumeChange();
    }

    public void OnEffectsSliderUpdate(Slider slider)
    {
        SetEffectsVolume(slider.value);
    }

    public void OnMusicSliderUpdate(Slider slider)
    {
        SetMusicVolume(slider.value);
    }

    public class AudioEventArgs : EventArgs
    {
        public float musicVolume { get; private set; }
        public float effectsVolume { get; private set; }
        public AudioEventArgs(float newMusicVolume, float newEffectsVolume)
        {
            musicVolume = newMusicVolume;
            effectsVolume = newEffectsVolume;
        }
    }
}
