﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
    public AudioSource synchroniserOnline;
    ActionSceneManager sceneManager;
	// Use this for initialization
	void Start () {
        sceneManager = GameObject.FindObjectOfType<ActionSceneManager>();
        sceneManager.OnSynchroniserCharged += SceneManager_OnSynchroniserCharged;
    }

    private void SceneManager_OnSynchroniserCharged(object sender, System.EventArgs e)
    {
        synchroniserOnline.Play();
    }

    // Update is called once per frame
    void Update () {
		
	}
}
