﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vehicle_SmallMotherShip : Vehicle {
    AudioSource audioSource;
    public GameObject destructionSequence;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public override void OnDeath()
    {
        //Convert this ship into debris:
        destructionSequence.SetActive(true); // enable the destruction sequence
        destructionSequence.transform.parent = null; // release it as its own object
        Destroy(gameObject);
    }

    public override void TakeDamage(DamageTypes damageType, int rawDamage)
    {
        int damageTaken = Vehicle.CalculateDamage(rawDamage, this.vehicleStatistics, damageType); // calculate damage taken using ordinary values
        this.vehicleStatistics.health -= damageTaken; // subtract this from our current health
        if(Vehicle.UiManager!=null)Vehicle.UiManager.SpawnHitMarker(damageTaken, gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z); // display the damage to the user
    }

    public override void OnCollision(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Wall")
        {
            if (!audioSource.isPlaying)
                audioSource.Play();
        }
    }
}