﻿using UnityEngine;
using System.Collections;

public class Vehicle_Movement : MonoBehaviour {
    bool stunnedFromHit;
    public float hitMovementCorrectionRate = .5f;

	public float accelerationRate;
	public float maxSpeed;
    public float rotationSpeed;
    public float breakingForceWeighting;

    public float wallBoundry;
	public float wallAvoidanceWeighting;
    public float goForwardsWeighting = 1f;

    Rigidbody2D rigidbody;
    [SerializeField] GameObject guidanceMarkerPrefab;
    GameObject guidanceMarker = null;
    // also toggles the visibility of the GuidanceMarker Object
    [SerializeField] private bool _showGuidanceMarker = false;
	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody2D>();
		stunnedFromHit = false;
        if (Debug.isDebugBuild) // guidance marker creation, only for debugging purposes
        {
            guidanceMarker = Instantiate<GameObject>(guidanceMarkerPrefab);
            guidanceMarker.transform.parent = transform;
            UpdateGuidanceMarkerVisibility();
        }
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (Debug.isDebugBuild)
            UpdateGuidanceMarkerVisibility();
		// update movement and rotation
		if (stunnedFromHit) {
			StopMoving ();
		} else {
            float rotationDelta = Rotation();
            Movement (rotationDelta);
        }
	}

    /// <summary>
    /// Slows the velocity of the Vehicle until standard movement can take over again
    /// </summary>
    void StopMoving()
    {
        rigidbody.velocity += -rigidbody.velocity.normalized * Time.deltaTime * hitMovementCorrectionRate;
        if (rigidbody.velocity.magnitude < .1f)
            stunnedFromHit = false;
    }

    /// <summary>
    /// Applies velocity relative to the current direction the vehicle is facing
    /// </summary>
    /// <param name="rotationDelta"></param>
	void Movement(float rotationDelta) {           
        // check if we are traveling above max speed
        if (rigidbody.velocity.magnitude > maxSpeed)
        {
            // apply brakes in the reverse direction to current velocity. Multiplied by a breakingWeighting and current velocity magnitude (so that super high velocites will still be dealt with quickly) 
            rigidbody.velocity += -rigidbody.velocity.normalized * Time.deltaTime * breakingForceWeighting * rigidbody.velocity.magnitude;
        }

        rigidbody.velocity += (-(Vector2)transform.up * Time.deltaTime * accelerationRate / rotationDelta); // move along ship forward (completely dependant on current rotation)
    }

    /// <summary>
    /// Rotates the Vehicle towards a desired travel direction.
    /// </summary>
    /// <returns></returns>
    float Rotation() {
        Vector3 towardsLeft = transform.position + Vector3.left;
        Vector3 awayFromWall = Vector3.zero;

        // calculate if we are too close to the wall boundrys (top or bottom) and if so get a vector pointing away from it
        if (transform.position.y > wallBoundry)
        {
            awayFromWall = -Vector2.up;
            awayFromWall.y += wallBoundry - transform.position.y;
        }
        else if (transform.position.y < -wallBoundry)
        {
            awayFromWall = Vector2.up;
            awayFromWall.y += -wallBoundry - transform.position.y;
        }

        Vector3 targetVector = towardsLeft * goForwardsWeighting + awayFromWall * wallAvoidanceWeighting; // add together direction vectors to get the target vector
        if (Debug.isDebugBuild) guidanceMarker.transform.position = targetVector; // Visualise the target position for debugging purposes

            Vector3 vectorToTarget = targetVector - transform.position;
        float angleTargetVector = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg; // calculate angle from current position to target vector
        // add additional rotating to compensate for facing direction offset
        angleTargetVector += 90f; // TODO: better way of defining facing direction
        Quaternion q = Quaternion.AngleAxis(angleTargetVector, Vector3.forward); // calculate quaternion rotation (hence avoiding 360 / 0 degree issues) taking us to desired angle around the forward (2D's 'Up' direction)


        float rotationDelta = Vector3.Angle(-transform.up, vectorToTarget); // gets absolute angle difference between current and target angle (accounts for 360 / 0 angle issues)
        if (rotationDelta < 1f) rotationDelta = 1f; // bound it to be always be 1 or greater
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * rotationSpeed / rotationDelta); // rotate towards desired direction

        return rotationDelta;
    }

    /// <summary>
    /// Updates the visibility of the Guidance Marker Object based on the value: _showGuidanceMarker
    /// </summary>
    void UpdateGuidanceMarkerVisibility()
    {
        if (Debug.isDebugBuild)
        {
            guidanceMarker.SetActive(_showGuidanceMarker);
        }
        else
        {
            print("The guidance marker doesn't exist outside of debug... How did you even turn this on?");
        }
    }

    /// <summary>
    /// Called by other that handle collisions to tell movement its been hit and should try to bring itself back under control
    /// </summary>
	public void Hit() {
		stunnedFromHit = true;
	}
}
	

