﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCatcher : MonoBehaviour {
    public ActionSceneManager sceneManager;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // enemy gets passed the player
        if(collision.gameObject.tag == "Enemy")
        {            
            sceneManager.GetPlayer().TakeDamage(20); // take damage
            Destroy(collision.gameObject); // stop the enemy vehicle going on forever
        }
    }
}
