﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Vehicle_Manager : MonoBehaviour {
	public Vector2 spawnLine;
	public GameObject[] vehicleList;
	
	public enum VehicleTypes{MotherShip, Small_MotherShip}; // easy referencing of spawn vehicles from list

    public float minRespawnTime = 8f;
    public float maxRespawnTime = 12f;
    private float spawnTimerCountdown;
    private bool isGameOver = false; // toggled on when player has gained enough power to win
    private bool isGameCompleted = false; // toggled on after player has won and also all enemies are destroyed

    public event EventHandler<VehicleEventArgs> OnVehicleSpawn; // Raised whenever a new vehicle is created, passes the vehicle
    public event EventHandler OnAllVehiclesDestroyed; // Raised once after player has won and all vehicles are destroyed

    // Use this for initialization
    void Start () {
		spawnTimerCountdown = 0;
        OnAllVehiclesDestroyed += Vehicle_Manager_OnAllVehiclesDestroyed;
	}

    private void Vehicle_Manager_OnAllVehiclesDestroyed(object sender, EventArgs e)
    {
        isGameCompleted = true;
    }

    // Update is called once per frame
    void Update () {
        if (!isGameOver) // while the game is not over
            SpawnCountdown(); // spawn vehicles
        else if(isGameOver && !isGameCompleted) // while game is won, but enemies are still alive
            TrackGameOver();
	}

    // check if the game has been completed and raise the appropriate event if that is the case
    void TrackGameOver()
    {
        GameObject[] vehicleList = GameObject.FindGameObjectsWithTag("Enemy");
        if(vehicleList.GetLength(0) == 0)
        {
            RaiseAllVehiclesDestroyed();
        }
    }

    // ticks down the respawn timer then instatiates a new enemy when it hits zero
    void SpawnCountdown()
    {
        spawnTimerCountdown -= Time.deltaTime; // decrement timer

        if (spawnTimerCountdown <= 0f) // countdown complete
        { 
            if (DiceRoller(1, 10) > 3)
                SpawnVehicle(VehicleTypes.Small_MotherShip); // 7 out of 10 chance of spawning
            else
                SpawnVehicle(VehicleTypes.MotherShip); // 3 / 10 chance of spawning

            spawnTimerCountdown = UnityEngine.Random.Range(minRespawnTime, maxRespawnTime); // reset timer
        }
    }

	public void SpawnVehicle (VehicleTypes newVehicle) {
        GameObject spawnedVehicle = null;
		switch(newVehicle)
		{
			case VehicleTypes.MotherShip:
                spawnedVehicle = (GameObject)Instantiate(vehicleList[0]);
                spawnedVehicle.transform.position = new Vector3(spawnLine.x, UnityEngine.Random.Range(-spawnLine.y,spawnLine.y), 0f);
				break;
            case VehicleTypes.Small_MotherShip:
                spawnedVehicle = (GameObject)Instantiate(vehicleList[1]);
                spawnedVehicle.transform.position = new Vector3(spawnLine.x, UnityEngine.Random.Range(-spawnLine.y, spawnLine.y), 0f);
                break;
        }

        // vehicle spawned, Raise the OnSpawn event
        if (spawnedVehicle != null)
            RaiseVehicleSpawned(spawnedVehicle.GetComponent<Vehicle>());
	}

    private void RaiseVehicleSpawned(Vehicle spawnedVehicle)
    {
        VehicleEventArgs eventArgs = new VehicleEventArgs(spawnedVehicle);
        var handler = OnVehicleSpawn;
        if (handler != null)
        {
            handler(this, eventArgs);
        }
    }

    private void RaiseAllVehiclesDestroyed()
    {
        var handler = OnAllVehiclesDestroyed;
        if (handler != null)
        {
            handler(this, EventArgs.Empty);
        }
    }

    /// <summary>
    /// Roll a given number of dice with a given number of sides and returns you the total (simple random number generator)
    /// </summary>
    /// <param name="numOfDice"></param>
    /// <param name="numOfSidesOnDice"></param>
    /// <returns></returns>
    int DiceRoller(int numOfDice, int numOfSidesOnDice)
    {
        int diceTotal = 0;
        for (int i = 0; i < numOfDice; i++)
        {
            diceTotal += UnityEngine.Random.Range(1, numOfSidesOnDice+1);
        }
        return diceTotal;
    }

	public List<GameObject> GetVehiclesInRange(float range, GameObject thisVehicle) {
		List<GameObject> returnList = new List<GameObject>(); // initialise return list

		foreach (GameObject aVehicle in GameObject.FindGameObjectsWithTag ("Enemy")) {
			float distance = Vector3.Distance(thisVehicle.transform.position, aVehicle.transform.position); // get distance between vehicles
			//print ("distance  " + distance);
			if(distance < range && aVehicle!=thisVehicle) // if in range and not the vehicle that requested
				returnList.Add (aVehicle);
		}

		return returnList;
	}

    public void VehicleManager_OnSynchroniserCharged(object sender, EventArgs e)
    {
        isGameOver = true; // player has won, stop spawning enemies
    }
}