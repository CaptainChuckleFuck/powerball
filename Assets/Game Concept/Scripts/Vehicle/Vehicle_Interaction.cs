
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System;

public class Vehicle_Interaction : MonoBehaviour {

	public VehicleStats thisVehicle;
	//public enum DamageTypes {Energy, Physical, Acid, Fire};
	//[System.Serializable]
	//public class VehicleStats {
	//	public int health;
	//	public int physicalArmour;
	//	public int energyArmour;
	//	public int acidArmour;
	//	public int fireArmour;
	//	public int speed;
	//	public int healthRegen;
	//	public int explosionPower;
	//	public float impactDamageMultiplier;
	//	public float angularVelocityMultiplier;
	//	public float massImpactMultiplier;
	//	public float deathTime;
	//	public bool isDying;
	//	public GameObject explosion;
	//	public GameObject vehiclePieces;
	//}

	private float wallInvinciblitlyTimer;
	// Use this for initialization
	void Start () {
		wallInvinciblitlyTimer = 0f;
	}
	
	// Update is called once per frame
	void Update () {
		wallInvinciblitlyTimer -= Time.deltaTime;

		//if (thisVehicle.isDying) {
		//	thisVehicle.deathTime -= Time.deltaTime;
		//	if(thisVehicle.deathTime <= 0f) {
  //              Die();
		//	}
		//}
	}

    /// <summary>
    /// Destroys this vehicle and implement any effects from its destruction
    /// </summary>
    void Die()
    {
        // get all vehicles in range from a globally held list of vehicles 
        //TODO: Cleanup this call
        List<GameObject> vehiclesInBlast = GameObject.FindGameObjectWithTag("Platform").GetComponent<Vehicle_Manager>().GetVehiclesInRange(thisVehicle.explosionPower, gameObject);
        foreach (GameObject aVehicle in vehiclesInBlast)
        {
            Vector2 awayVector = aVehicle.GetComponent<Rigidbody2D>().position - GetComponent<Rigidbody2D>().position; // vector pointing from dying vehicle to other one
            float hitPower = 1 / awayVector.magnitude; // linear scaling of power based on distance
            aVehicle.GetComponent<Rigidbody2D>().AddForce(awayVector.normalized * hitPower * thisVehicle.explosionPower * 800f); // normalized vector scaled by distance and based power
            //print (awayVector.normalized * hitPower * thisVehicle.explosionPower);
        }
        //thisVehicle.explosion.transform.parent = null; // deparent explosion
        //Destroy(thisVehicle.explosion, 1.2f); // set explosion to die after it finishes its animation
        //thisVehicle.vehiclePieces.transform.parent = null; // release any debris pieces that explosion creates
        //thisVehicle.vehiclePieces.GetComponent<Vehicle_Destruction>().VehicleDies(); // Hand over all control of this vehicle to the destrucion script, start its death sequence
        //EditorApplication.isPaused = true;        
        Destroy(gameObject);
    }

	public void DamageVehicle (int damage, DamageTypes damageType) {

		//switch (damageType)
		//{
		//	case DamageTypes.Energy:
		//		damage -= Mathf.FloorToInt(thisVehicle.energyArmour + thisVehicle.physicalArmour/3f);
		//		damage = Mathf.Clamp(damage, 1, thisVehicle.health);
		//		GameObject.FindGameObjectWithTag ("UI").GetComponent<UI_Manager>().SpawnHitMarker(damage, gameObject.transform.position);//TODO: cleanup manager calls
		//		thisVehicle.health -= damage;
		//		break;
		//	case DamageTypes.Physical:
		//		damage -= thisVehicle.physicalArmour;
		//		damage = Mathf.Clamp(damage, 1, thisVehicle.health);
		//		GameObject.FindGameObjectWithTag ("UI").GetComponent<UI_Manager>().SpawnHitMarker(damage, gameObject.transform.position);
		//		thisVehicle.health -= damage;
		//		break;
		//	case DamageTypes.Acid:
		//		break;
		//	case DamageTypes.Fire:
		//		break;
		//}

		if (thisVehicle.health <= 0) {// no health left
            StartDeathSequence();
        }
	}

    void StartDeathSequence()
    {
        //thisVehicle.isDying = true;
        //if (thisVehicle.explosion != null)
        //    try
        //    {
        //        thisVehicle.explosion.SetActive(true); //HERE
        //    } 
        //    catch(Exception e)
        //    {
        //        print("Fail");
        //    }
        //if(thisVehicle.vehiclePieces != null)
        //    thisVehicle.vehiclePieces.GetComponent<Vehicle_Destruction>().BeginDying(thisVehicle.deathTime);
    }

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.collider.gameObject.tag == "Enemy") {
			Vector2 collisionForce = GetComponent<Rigidbody2D>().velocity - coll.gameObject.GetComponent<Rigidbody2D>().velocity;
			VehicleStats otherVehicle = coll.gameObject.GetComponent<Vehicle>().vehicleStatistics;
			float angVel;
			angVel = Mathf.Abs(GetComponent<Rigidbody2D>().angularVelocity - coll.gameObject.GetComponent<Rigidbody2D>().angularVelocity);

			// damage is (magnitude of current velocities)  * (mass of object hit by) * (combined angular velocity) * (overall damage multiplier [if other vehicle has spikes this will be higher for instance])
			DamageVehicle(Mathf.FloorToInt(collisionForce.magnitude * (coll.gameObject.GetComponent<Rigidbody2D>().mass * otherVehicle.massImpactMultiplier) * otherVehicle.impactDamageMultiplier + (angVel*otherVehicle.angularVelocityMultiplier))
			              	,DamageTypes.Physical);
			print ("My Name : " + gameObject.name + "      Force Magnitude :  " + collisionForce.magnitude + "      Mass : " + coll.gameObject.GetComponent<Rigidbody2D>().mass*otherVehicle.massImpactMultiplier + "\nAngular Velocity : " + angVel*otherVehicle.angularVelocityMultiplier + "      Total" + Mathf.FloorToInt(collisionForce.magnitude * (coll.gameObject.GetComponent<Rigidbody2D>().mass * otherVehicle.massImpactMultiplier) * otherVehicle.impactDamageMultiplier + (angVel*otherVehicle.angularVelocityMultiplier)));
			//print ("My Vel : " + rigidbody2D.velocity + "   His Vel : " + coll.gameObject.rigidbody2D.velocity + "   Coll Force : "  + collisionForce + "\n Force Magnitude : " + collisionForce.magnitude);
		}
		else if (coll.collider.gameObject.tag == "Wall" && wallInvinciblitlyTimer <= 0f) {
            // damage = speed we hit at * our weight * how fast we are spinning (weighted)
			int rawDamage = Mathf.FloorToInt(GetComponent<Rigidbody2D>().velocity.magnitude * GetComponent<Rigidbody2D>().mass * GetComponent<Rigidbody2D>().angularVelocity * 0.5f);
			// min damage is 1. max damage is the objects own mass
			rawDamage = Mathf.Clamp(rawDamage,1,Mathf.CeilToInt(GetComponent<Rigidbody2D>().mass));
			DamageVehicle(rawDamage,DamageTypes.Physical); // deliver calculated damage to the vehicle as raw physical
			wallInvinciblitlyTimer = 2f; // seconds of invincibility
		}
		else if (coll.collider.gameObject.tag == "Ball") {
			GetComponent<Vehicle_Movement>().Hit ();
		}
	}
}
