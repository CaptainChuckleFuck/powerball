﻿
using System;
using UnityEngine;
//TODO: Follow design pattern used for Ball and Ball_Interaction. Need a seperate events passer in order to handle OnDeath and OnExplosion seperately (maybe an interface for explodes)
public abstract class Vehicle : MonoBehaviour 
{    
    public VehicleStats vehicleStatistics;
    public static UI_Manager UiManager;
    //public Vehicle_Movement movementController; //TODO: abstract movement?

    float wallInvinciblitlyTimer = 0f;
    public int scoreValue = 0;

    /// <summary>
    /// Called when this vehicles health goes under 0. Implementations should finish up by removing this script or adding health to bring the vehicle back to life.
    /// </summary>
    public abstract void OnDeath();
    public abstract void TakeDamage(DamageTypes damageType, int rawDamage);
    public abstract void OnCollision(Collision2D collision);
    public event EventHandler<VehicleEventArgs> OnVehicleDeath;

    void Update()
    {
        //TODO: Add movement update
        wallInvinciblitlyTimer -= Time.deltaTime;
        if (vehicleStatistics.health <= 0)
        {
            RaiseDeathEvent();
            this.OnDeath();
        }        
    }

    // Calls the Death Event for this vehicle
    private void RaiseDeathEvent()
    {
        var handler = OnVehicleDeath;
        if (handler != null)
        {
            handler(this, new VehicleEventArgs(this));
        }
    }

    /// <summary>
    /// Global damage reduction calculator. Takes a raw damage value and the type of damange value dealt then calculates a reduced value for a given set of defensive statistics
    /// </summary>
    /// <param name="rawDamage"></param>
    /// <param name="vehicleStatistics"></param>
    /// <param name="damageType"></param>
    /// <returns></returns>
    public static int CalculateDamage(int rawDamage, VehicleStats vehicleStatistics, DamageTypes damageType)
    {
        int damageTaken = rawDamage;

        switch (damageType)
        {
            case DamageTypes.Energy:
                damageTaken -= Mathf.FloorToInt(vehicleStatistics.energyArmour + vehicleStatistics.physicalArmour / 3f); // subtract energy armour + 1 / 3 of physical armour from damage
                break;
            case DamageTypes.Physical:
                damageTaken -= vehicleStatistics.physicalArmour;         
                break;
            case DamageTypes.Acid:
                break;
            case DamageTypes.Fire:
                break;
        }

        // clamp it to do a minimum of 1 damage and at maximum all of its remaining health 
        damageTaken = Mathf.Clamp(damageTaken, 1, vehicleStatistics.health); 
        return damageTaken;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        // handle hits from all possible sources
        if (coll.collider.gameObject.tag == "Enemy")
        {
            Vector2 collisionForce = GetComponent<Rigidbody2D>().velocity - coll.gameObject.GetComponent<Rigidbody2D>().velocity;
            VehicleStats otherVehicle = coll.gameObject.GetComponent<Vehicle>().vehicleStatistics;
            float angVel = Mathf.Abs(GetComponent<Rigidbody2D>().angularVelocity - coll.gameObject.GetComponent<Rigidbody2D>().angularVelocity);
            // damage is (magnitude of current velocities)  * (mass of object hit by) * (combined angular velocity) * (overall damage multiplier [if other vehicle has spikes this will be higher for instance])
            int rawDamage = Mathf.FloorToInt(collisionForce.magnitude * (coll.gameObject.GetComponent<Rigidbody2D>().mass * otherVehicle.massImpactMultiplier) * otherVehicle.impactDamageMultiplier + (angVel * otherVehicle.angularVelocityMultiplier));
            TakeDamage(DamageTypes.Physical, rawDamage);
                
            //print("My Name : " + gameObject.name + "      Force Magnitude :  " + collisionForce.magnitude + "      Mass : " + coll.gameObject.GetComponent<Rigidbody2D>().mass * otherVehicle.massImpactMultiplier + "\nAngular Velocity : " + angVel * otherVehicle.angularVelocityMultiplier + "      Total" + Mathf.FloorToInt(collisionForce.magnitude * (coll.gameObject.GetComponent<Rigidbody2D>().mass * otherVehicle.massImpactMultiplier) * otherVehicle.impactDamageMultiplier + (angVel * otherVehicle.angularVelocityMultiplier)));
            //print ("My Vel : " + rigidbody2D.velocity + "   His Vel : " + coll.gameObject.rigidbody2D.velocity + "   Coll Force : "  + collisionForce + "\n Force Magnitude : " + collisionForce.magnitude);
        }
        else if (coll.collider.gameObject.tag == "Wall" && wallInvinciblitlyTimer <= 0f)
        {
            // damage = speed we hit at * our weight * how fast we are spinning (weighted)
            int rawDamage = Mathf.FloorToInt(GetComponent<Rigidbody2D>().velocity.magnitude * GetComponent<Rigidbody2D>().mass * GetComponent<Rigidbody2D>().angularVelocity * 0.5f);
            // min damage is 1. max damage is the objects own mass
            rawDamage = Mathf.Clamp(rawDamage, 1, Mathf.CeilToInt(GetComponent<Rigidbody2D>().mass));
            TakeDamage(DamageTypes.Physical, rawDamage); // deliver calculated damage to the vehicle as raw physical
            wallInvinciblitlyTimer = 2f; // seconds of invincibility
        }
        else if (coll.collider.gameObject.tag == "Ball")
        {
            Ball_Interaction hitBall = coll.gameObject.GetComponent<Ball_Interaction>();
            GetComponent<Vehicle_Movement>().Hit();
            TakeDamage(hitBall.baseDamageType, hitBall.ballDamage);
        }

        OnCollision(coll);
    }
}

public class VehicleEventArgs : EventArgs
{
    public Vehicle TheVehicle { get; private set; }

    public VehicleEventArgs(Vehicle vehicle)
    {
        TheVehicle = vehicle;
    }
}
