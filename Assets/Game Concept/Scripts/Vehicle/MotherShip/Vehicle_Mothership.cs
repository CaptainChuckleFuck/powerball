﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vehicle_Mothership : Vehicle {
    public float deathTime;
    public bool isDying;    
    public GameObject destructionSequence;
    AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public override void OnDeath()
    {
        //Convert this ship into debris:
        destructionSequence.SetActive(true); // enable the destruction sequence
        destructionSequence.transform.parent = null; // release it as its own object
        Destroy(this.gameObject); // destroy ourselves
    }

    public override void TakeDamage(DamageTypes damageType, int rawDamage)
    {
        int damageTaken = Vehicle.CalculateDamage(rawDamage, this.vehicleStatistics, damageType);
        if (damageTaken < 0) print("DamageTaken: " + damageTaken);
        
        this.vehicleStatistics.health -= damageTaken;
        UiManager.SpawnHitMarker(damageTaken, gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
    }

    public override void OnCollision(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Wall")
        {
            if (!audioSource.isPlaying)
                audioSource.Play();
        }
    }
}