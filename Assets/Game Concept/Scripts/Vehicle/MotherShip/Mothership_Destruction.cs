﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mothership_Destruction : MonoBehaviour {

    public GameObject[] debrisPieces;
    public float debrisLifetime; // how long the debris pieces exist after being released
    float transparencyIncrement;
    float deathTimer;
    public float timeToExplosion; // time taken for death sequence to complete
    bool isDying;
    public GameObject explosion;

    // Use this for initialization
    void Start()
    {
        deathTimer = 0;
        BeginDying();
    }

    public void BeginDying()
    {
        isDying = true;
        transparencyIncrement = 1f / timeToExplosion;
        // pieces dissapear after vehicle death time + their own lifetime
        Destroy(gameObject, debrisLifetime + timeToExplosion);
        explosion.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        // if death sequence has completed
        if (deathTimer >= timeToExplosion && isDying)
        {
            isDying = false;
            ExplodeShip();
        }
        else if (isDying) // while going through death sequence
        {
            deathTimer += Time.deltaTime; // increment timer
            // fade in damage on buildup to explosion
            foreach (GameObject aPiece in debrisPieces) 
            {
                float transparency = transparencyIncrement * deathTimer;

                Color aPieceColour = aPiece.GetComponent<SpriteRenderer>().color;
                aPieceColour.a = transparency;
                //print (aPieceColour);
                aPiece.GetComponent<SpriteRenderer>().color = aPieceColour;
            }
        }

    }

    void ExplodeShip()
    {
        HideUnexplodedShip();
        // release effects
        ReleaseExplosion();
        ReleaseDebris();
        Destroy(this); // sequence completed, destroy this script
    }

    void HideUnexplodedShip()
    {
        // turn off physics and visuals
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        GetComponent<Rigidbody2D>().simulated = false;
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<Collider2D>().enabled = false;
    }

    void ReleaseDebris()
    {
        foreach (GameObject aPiece in debrisPieces)
        {            
            Rigidbody2D pieceRigidbody = aPiece.GetComponent<Rigidbody2D>();
            pieceRigidbody.simulated = true;
            pieceRigidbody.AddForce(new Vector2(Random.Range(-5f, 5f), Random.Range(-5f, 5f)));
            pieceRigidbody.angularVelocity = Random.Range(-30f, 30f);
            aPiece.GetComponent<PolygonCollider2D>().enabled = true;
        }
    }

    void ReleaseExplosion()
    {
        explosion.transform.parent = null;
        Destroy(explosion, 1f);
    }    
}
