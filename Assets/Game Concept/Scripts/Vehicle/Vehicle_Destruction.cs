﻿using UnityEngine;
using System.Collections;

public class Vehicle_Destruction : MonoBehaviour {
	public GameObject[] vehiclePieces;
	public float pieceLifetime;
	float transparencyIncrement;
	float deathTimer;
	float timeToDie;
	bool isDying;

	// Use this for initialization
	void Start () {
		deathTimer = 0;
		isDying = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (deathTimer >= timeToDie && isDying) {
            isDying = false;
            Explode();
        }
        else if (isDying){
			deathTimer += Time.deltaTime;
			foreach (GameObject aPiece in vehiclePieces) {
				float transparency = transparencyIncrement*deathTimer;

				Color aPieceColour = aPiece.GetComponent<SpriteRenderer>().color;
				aPieceColour.a = transparency;
				//print (aPieceColour);
				aPiece.GetComponent<SpriteRenderer>().color = aPieceColour;
			}
		}

	}

    // 
	public void Explode () {
		foreach (GameObject aPiece in vehiclePieces) {
			aPiece.AddComponent<Rigidbody2D>();
			aPiece.GetComponent<Rigidbody2D>().gravityScale = 0f;
			aPiece.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range (-50f,50f),Random.Range (-50f,50f)));
			aPiece.GetComponent<Rigidbody2D>().angularVelocity = Random.Range (-30f,30f);
			aPiece.GetComponent<PolygonCollider2D>().enabled = true;
		}
	}

	public void BeginDying (float deathTime) {
		timeToDie = deathTime;
		isDying = true;
		transparencyIncrement = 1f / deathTime;
		// pieces dissapear after vehicle death time + their own lifetime
		Destroy (gameObject,pieceLifetime+deathTime);
	}
}
