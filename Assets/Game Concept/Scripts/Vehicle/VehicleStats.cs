﻿[System.Serializable]
public class VehicleStats {    
    public int health;
    public int physicalArmour;
    public int energyArmour;
    public int acidArmour;
    public int fireArmour;
    public int speed;
    public int healthRegen;
    public int explosionPower;
    public float impactDamageMultiplier;
    public float angularVelocityMultiplier;
    public float massImpactMultiplier;    
}

public enum DamageTypes { Energy, Physical, Acid, Fire };
