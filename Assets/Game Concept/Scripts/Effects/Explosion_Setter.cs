﻿using UnityEngine;
using System.Collections;

public class Explosion_Setter : MonoBehaviour {
    public AudioClip buildupSound;
    public AudioClip explosionSound;
    public float explosionDelay = 2f;
    AudioSource audioSource;
    bool hasExploded = false;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = buildupSound;
        audioSource.Play(); // play the buildup sound
    }

    private void Update()
    {
        explosionDelay -= Time.deltaTime;
        if(explosionDelay <= 0 && !hasExploded) // swap the two sounds when countdown has been reached
        {
            audioSource.Stop();
            audioSource.clip = explosionSound;
            audioSource.Play();
            hasExploded = true;
        }
    }
}
