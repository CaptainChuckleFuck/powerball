﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SynchTextEffect : MonoBehaviour {
    [SerializeField] private float hiddenTime;
    [SerializeField] private float shownTime;
    bool isShown = true;
    private float countdownTimer = 0;

    // Use this for initialization
    void Awake () {
        countdownTimer = shownTime;
        isShown = true;
    }
	
	// Update is called once per frame
	void Update () {
        countdownTimer -= Time.deltaTime;
                
        if (countdownTimer <= 0) // countdown and toggle text from on / off to create blinking effect
            ToggleVisibility();

    }

    void ToggleVisibility()
    {
        if (isShown)
            HideText();
        else
            ShowText();
    }

    void HideText()
    {        
        countdownTimer = hiddenTime;
        GetComponent<Text>().enabled = false;
        isShown = false;
    }

    void ShowText()
    {
        countdownTimer = shownTime;
        GetComponent<Text>().enabled = true;
        isShown = true;
    }
}
