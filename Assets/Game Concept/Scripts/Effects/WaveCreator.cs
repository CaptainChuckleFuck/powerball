﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveCreator : MonoBehaviour {
    public GameObject wavePrefab;
    [SerializeField] float timeBetweenWaves;
    [SerializeField] float waveLifeTime;
    float waveCountdownTimer;
    bool isActive = false;
	// Use this for initialization
	void Start () {
        GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().OnSynchroniserCharged += WaveCreator_OnSynchroniserCharged;
        GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().OnGameOver += WaveCreator_OnGameOver;
    }

    private void WaveCreator_OnGameOver(object sender, EventArgs e)
    {
        isActive = false;
    }

    private void WaveCreator_OnSynchroniserCharged(object sender, EventArgs e)
    {
        isActive = true;
    }

    // Update is called once per frame
    void Update () {
        if (isActive)
            SpawnWaves();
        
	}

    void SpawnWaves()
    {
        waveCountdownTimer -= Time.deltaTime;

        if (waveCountdownTimer <= 0) // if new wave should spawn
        {
            waveCountdownTimer = timeBetweenWaves; // reset timer
            GameObject newWave = (GameObject)Instantiate(wavePrefab, transform.position, Quaternion.identity); // spawn wave at this creators position
            Destroy(newWave, waveLifeTime); // destroy the wave after its life is over
        }
    }
}
