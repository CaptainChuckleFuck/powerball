﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnFire : MonoBehaviour {
    [SerializeField] float lifeTime = 3f;
    [SerializeField] int damagePerSecond = 5;
    [SerializeField] Vehicle effectedVehicle;
    [SerializeField] static GameObject fireEffectPrefab;
    float perSecondCountdown = 1; // counter triggers once per second
    GameObject fireEffect;

    public void StartFire(GameObject fireVisualsPrefab)
    {
        fireEffectPrefab = fireVisualsPrefab;
        perSecondCountdown = 1f;
        GameObject newFlame = (GameObject)Instantiate(fireEffectPrefab, gameObject.transform);
        newFlame.transform.position = gameObject.transform.position;
        effectedVehicle = GetComponent<Vehicle>();
        Destroy(this, lifeTime); // 
        Destroy(newFlame, lifeTime); // stop visuals after effect is over
    }

    private void Update()
    {
        perSecondCountdown -= Time.deltaTime;
        if (perSecondCountdown <= 0f)
        {
            effectedVehicle.TakeDamage(DamageTypes.Fire, 5);
            perSecondCountdown = 1f;
        }
    }

    // Spread the flames to other enemies on contact
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Enemy")
        {
            if(collision.gameObject.GetComponent<OnFire>() == null) // if its not already on fire
            {
                OnFire newFire = collision.gameObject.AddComponent<OnFire>(); // spread the flames
                newFire.StartFire(fireEffectPrefab);
            }
        }
    }
}
