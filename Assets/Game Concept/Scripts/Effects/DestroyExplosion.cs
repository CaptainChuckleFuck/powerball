﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyExplosion : MonoBehaviour {
    public float deletionDelay = 2f;
	// Use this for initialization
	void Start () {
        Destroy(gameObject, deletionDelay);
	}
}
