﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneVibration : MonoBehaviour {
    [SerializeField] BallLauncher ballLauncher;
    bool holdingBall = false;

	// Use this for initialization
	void Start () {
        // subscribe to the launchers events
        ballLauncher.OnBallPull += BallLauncher_OnBallPull;
        ballLauncher.OnBallRelease += BallLauncher_OnBallRelease; 
    }

    private void BallLauncher_OnBallRelease(object sender, System.EventArgs e)
    {
        holdingBall = false;
    }

    private void BallLauncher_OnBallPull(object sender, System.EventArgs e)
    {
        holdingBall = true;
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (holdingBall) // while the user is interacting with the ball, vibrate the device
            Vibrate();
	}

    void Vibrate()
    {
#if !UNITY_STANDALONE_WIN
        Handheld.Vibrate(); // if this is on standalone windows, do not try to use the Vibrate function
#endif
    }
}
