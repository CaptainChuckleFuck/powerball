﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour {
    [SerializeField] Ball_Manager ballManager;
    [SerializeField] Vehicle_Manager vehicleManager;

    [SerializeField] float stabilizationRate;
    [SerializeField] float movementMultiplier;    
    [SerializeField] float wallHitShakeMultiplier;
    [SerializeField] float explosionShakeMultiplier;
    Vector3 currentShakeVelocity = Vector3.zero;
    Vector3 cameraStartLocation;
	// Use this for initialization
	void Start () {
        cameraStartLocation = transform.position;
        if (ballManager != null)
            ballManager.OnBallSpawn += BallManager_OnBallSpawn;
        else
            Debug.LogWarning("No Ball Manager Found by CameraShake");

        if (vehicleManager != null)
            vehicleManager.OnVehicleSpawn += VehicleManager_OnVehicleSpawn;
        else
            Debug.LogWarning("No Vehicle Manager Found by CameraShake");
    }

    // when a new vehicle is spawned
    private void VehicleManager_OnVehicleSpawn(object sender, VehicleEventArgs e) 
    {
        //subscribe to know when it dies
        e.TheVehicle.OnVehicleDeath += TheVehicle_OnVehicleDeath; 
    }

    // when a vehicles dies
    private void TheVehicle_OnVehicleDeath(object sender, VehicleEventArgs e) 
    {
        // get a direction pointing towards the camera (ignoring the Z axis)
        Vector2 toCamera = gameObject.transform.position - e.TheVehicle.gameObject.transform.position;
        AddShake(toCamera.normalized * explosionShakeMultiplier); // add shake like the camera is pushed away by the explosion
    }

    private void BallManager_OnBallSpawn(object sender, System.EventArgs e)
    {
        // subscribe to know when the ball hits somethings
        ballManager.unreleasedBall.GetComponent<Ball_Interaction>().OnBallCollision += CameraShake_OnBallCollision; 
    }

    private void CameraShake_OnBallCollision(object sender, Ball_Interaction.CollisionEventArgs e)
    {
        if(e.collision.gameObject.tag == "Wall")
        {
           Vector2 ballVelocity = (sender as Ball_Interaction).gameObject.GetComponent<Rigidbody2D>().velocity;
           AddShake(ballVelocity.normalized * wallHitShakeMultiplier);
        }
    }

    // Update is called once per frame
    void FixedUpdate () {
		if(currentShakeVelocity.sqrMagnitude != 0)
        {
            MoveCamera(); // moves the camera, slerping from cameras defualt position and default position + shake velocity
            ReduceShake(); // reduces shake for next frame
        }
	}

    void MoveCamera()
    {
        // move the camera over time towards the offset position caused by camera shake
        transform.position = Vector3.Slerp(cameraStartLocation + currentShakeVelocity, cameraStartLocation, Time.deltaTime * movementMultiplier);
    }

    void ReduceShake()
    {
        // lower the shake offset over time towards zero
        currentShakeVelocity = Vector3.Slerp(currentShakeVelocity, Vector2.zero, Time.deltaTime * stabilizationRate); 
    }

    public void AddShake(Vector3 cameraShake)
    {
        currentShakeVelocity += cameraShake;
    }
}
