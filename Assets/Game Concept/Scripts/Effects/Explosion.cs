﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {
    [SerializeField] float explosionRadius = 2f;
    [SerializeField] float forceMultiplier = 2f;
    [SerializeField] float baseDamage = 10f;
    [SerializeField] float damageFalloffRate = 5f; // how much damage to subtract from base per 1 Unity Unit away from center
    [SerializeField] float countdownToExplosion = 2f;
    public List<string> effectedTags;
    Dictionary<GameObject, float> objectsInRange = new Dictionary<GameObject, float>();

    public AudioClip buildupSound;
    public AudioClip explosionSound;
    AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = buildupSound;
        audioSource.Play(); // starts a looping windup sound for the explosion
    }

    // Update is called once per frame
    void Update () {
        countdownToExplosion -= Time.deltaTime;

        if (countdownToExplosion <= 0)
        {
            PlayExplosionSound();
            Explode();
        }
    }

    void PlayExplosionSound()
    {
        audioSource.clip = explosionSound;
        audioSource.loop = false;
        audioSource.Play();
    }

    void Explode()
    {
        // compile list of effected objects in range of explosion
        foreach(string effectedType in effectedTags)
        {
            foreach(GameObject effectedObject in GameObject.FindGameObjectsWithTag(effectedType))
            {
                float distanceToObject = Vector3.Distance(gameObject.transform.position, effectedObject.transform.position);
                if (distanceToObject < explosionRadius && effectedObject != gameObject)
                {
                    objectsInRange.Add(effectedObject, distanceToObject);
                }
                //print("Object: " + effectedObject.name + " - Distance: " + distanceToObject);
            }
        }

        // loops through all effected objects in blast radius and do stuff to them
        foreach(GameObject objectInRange in objectsInRange.Keys)
        {
            Vehicle aVehicle = objectInRange.GetComponent<Vehicle>();
            if (aVehicle != null) // if its a type of vehicle deliver damage
            {
                DamageVehicle(aVehicle, objectsInRange[objectInRange]);
            }
            ApplyPhysics(objectInRange, objectsInRange[objectInRange]); // apply some explosion push (or pull if you fancy) to the effected object
        }

        Destroy(this);
    }

    void DamageVehicle(Vehicle vehicle, float distanceToVehicle)
    {
        float damage = baseDamage - distanceToVehicle * damageFalloffRate;
        vehicle.TakeDamage(DamageTypes.Fire, Mathf.FloorToInt(damage));
        vehicle.gameObject.GetComponent<Vehicle_Movement>().Hit();
    }

    void ApplyPhysics(GameObject effectedObject, float distanceToEffectedObject)
    {
        Rigidbody2D rigidbody = effectedObject.GetComponent<Rigidbody2D>();
        if (rigidbody != null)
        {
            Vector2 explosionDirection = (Vector2)(effectedObject.transform.position - transform.position);
            explosionDirection.Normalize();
            if(explosionDirection.sqrMagnitude != 0) // quick test that these two objects are not directly on top of one another
                rigidbody.AddForce(explosionDirection * forceMultiplier / distanceToEffectedObject);            
        }
    }
}
