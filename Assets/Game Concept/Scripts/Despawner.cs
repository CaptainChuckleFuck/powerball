﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// destroys any enemies that go to far behind the spawn point
public class Despawner : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Enemy")
        {
            Destroy(collision.gameObject);
        }
    }
}
