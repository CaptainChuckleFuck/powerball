﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialHand : MonoBehaviour {
    [SerializeField] Sprite unpressedTexture;
    [SerializeField] Sprite pressedTexture;
    [SerializeField] SpriteRenderer spriteRenderer;
    bool isPressed = false;
    [SerializeField] float moveSpeed = 5f;
    Vector3 targetLocation = Vector3.zero;
    bool isInitialized = false;
	// Use this for initialization
	void Start () {
        Initialize();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        // move towards target
        transform.position = Vector3.Lerp(transform.position, targetLocation, Time.deltaTime * moveSpeed);

	}

    public void Initialize()
    {
        if(!isInitialized)
        {
            isInitialized = true;
            targetLocation = transform.position;
        }
    }

    public void SetTarget(Vector3 newTarget)
    {
        newTarget.z = targetLocation.z; // ensure our Z never changes
        targetLocation = newTarget;
    }

    // switch between looking pressed and unpressed
    public void TogglePressedState() 
    {
        if (isPressed)
            spriteRenderer.sprite = unpressedTexture;
        else
            spriteRenderer.sprite = pressedTexture;

        isPressed = !isPressed;
    }

    public bool GetPressedState()
    {
        return isPressed;
    }
}
