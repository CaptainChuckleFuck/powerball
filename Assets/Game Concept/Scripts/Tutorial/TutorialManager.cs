﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialManager : GameManager {
    [SerializeField] GameObject tutorialHandPrefab;
    TutorialHand tutorialHand;
    [SerializeField] Transform ballSpawnPosition;
    [SerializeField] Transform handSpawnPosition;
    [SerializeField] Transform pullBackPosition;
    [SerializeField] Transform pullBackPositionAlternate;
    [SerializeField] GameObject trackedObject;
    [SerializeField] GameObject[] sampleEnemies;
    [SerializeField] GameObject pullInstructionsBasics;
    [SerializeField] GameObject pullInstructionsEnemies;
    GameObject theHand;
    float waitTimer = 0f;
    [SerializeField] TutorialLauncher tutorialLauncher;
    int tutorialSequenceStage = 0;
	// Use this for initialization
	void Start () {
        theHand = Instantiate(tutorialHandPrefab, handSpawnPosition.transform.position, Quaternion.identity) as GameObject;        
        tutorialHand = theHand.GetComponent<TutorialHand>();
        tutorialHand.Initialize();
    }
	
	// Update is called once per frame
	void Update () {
        tutorialHand.SetTarget(trackedObject.transform.position);
        TutorialSequence();
    }

    void TutorialSequence()
    {
        switch (tutorialSequenceStage)
        {
            case 0:
                ReachedBall();
                break;
            case 1:
                PulledBallBack();
                break;
            case 2:
                ResetPullSequence();
                break;
            case 3:
                EnableEnemyText();
                break;
            case 4:
                Wait();
                break;
            case 5:
                ReachedBall();
                break;
            case 6:
                SpawnEnemies();
                break;
            case 7:
                PulledBallBack();
                break;
            case 8:
                waitTimer = 2f;
                tutorialSequenceStage++;
                break;
            case 9:
                Wait();
                break;
            case 10:
                ExitScene();
                break;
            default:
                break;
        }
    }

    void ExitScene()
    {
        SceneManager.LoadScene("Menu");
    }

    void EnableEnemyText()
    {
        pullInstructionsBasics.SetActive(false);
        pullInstructionsEnemies.SetActive(true);
        waitTimer = 3f;
        tutorialSequenceStage++;
    }

    void ResetPullSequence()
    {
        pullBackPosition = pullBackPositionAlternate; // set to shoot straight this time
        trackedObject.transform.position = ballSpawnPosition.position;
        tutorialSequenceStage++;
    }

    void SpawnEnemies()
    {
        foreach (GameObject anEnemy in sampleEnemies)
        {
            anEnemy.SetActive(true);
        }
        tutorialSequenceStage++;
    }

    void ReachedBall()
    {
        if (Vector2.Distance(trackedObject.transform.position, tutorialHand.gameObject.transform.position) < .2f)
        {
            tutorialLauncher.SetFollowObject(theHand.gameObject);
            tutorialHand.TogglePressedState();
            trackedObject.transform.position = pullBackPosition.position;
            tutorialSequenceStage++;
        }
    }

    void PulledBallBack()
    {
        if (Vector2.Distance(trackedObject.transform.position, tutorialHand.gameObject.transform.position) < .2f)
        {
            tutorialLauncher.StopFollowObject();
            tutorialHand.TogglePressedState();
            tutorialSequenceStage++;
        }
    }

    void Wait()
    {
        waitTimer -= Time.deltaTime;
        if(waitTimer <= 0)
        {
            tutorialSequenceStage++;
        }
    }
}