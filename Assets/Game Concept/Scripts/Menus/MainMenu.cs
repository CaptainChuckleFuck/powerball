﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip buttonPressedSound;
    [SerializeField] GameObject optionsMenu;
    [SerializeField] GameObject storyDisplay;
    public void Options()
    {
        ButtonPressed();
        optionsMenu.SetActive(true); // toggle options UI on
        gameObject.SetActive(false); // toggle main buttons off
    }

    public void Story()
    {
        ButtonPressed();
        storyDisplay.SetActive(true); // toggle options UI on
        gameObject.SetActive(false); // toggle main buttons off
    }

    public void ChangeScene(string sceneName)
    {
        ButtonPressed();
        SceneManager.LoadScene(sceneName);
    }

    public void CloseGame()
    {
        ButtonPressed();
        Application.Quit();
    }

    /// <summary>
    /// Any special effects related to the button being press
    /// </summary>
    void ButtonPressed()
    {
        audioSource.clip = buttonPressedSound;
        audioSource.Play();
    }
}
