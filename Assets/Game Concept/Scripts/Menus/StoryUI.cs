﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryUI : MonoBehaviour {
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip buttonPressedSound;
    [SerializeField] GameObject storyUI;
    [SerializeField] GameObject mainMenuUI;
	public void BackButton()
    {
        ButtonPressed();
        mainMenuUI.SetActive(true); // toggle  on main buttons
        storyUI.SetActive(false); // toggle Story UI off
    }

    /// <summary>
    /// Any special effects related to the button being press
    /// </summary>
    void ButtonPressed()
    {
        audioSource.clip = buttonPressedSound;
        audioSource.Play();
    }
}
