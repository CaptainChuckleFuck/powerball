﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour {
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip buttonPressedSound;
    [SerializeField] GameObject mainMenu;
    [SerializeField] Slider musicSlider;
    [SerializeField] Slider effectsSlider;

    void Start()
    {
        SyncFromPrefs();
    }

    /// <summary>
    /// Gets Values from player prefs and loads them into the AudioManager
    /// </summary>
    public void SyncFromPrefs()
    {
        if (PlayerPrefs.HasKey("Music_Volume")) // check if key exists
        {
            musicSlider.value = PlayerPrefs.GetFloat("Music_Volume"); // update
        }

        if (PlayerPrefs.HasKey("Effects_Volume")) // check if key exists
        {
            effectsSlider.value = PlayerPrefs.GetFloat("Effects_Volume"); // update value from prefs
        }
    }

    public void BackButton()
    {
        PlayButtonSound();
        mainMenu.SetActive(true); // toggle main menu UI on
        gameObject.SetActive(false); // toggle options off
    }

    void PlayButtonSound()
    {
        audioSource.clip = buttonPressedSound;
        audioSource.Play();
    }
}
