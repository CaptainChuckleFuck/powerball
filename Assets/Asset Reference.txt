Game Concept/Textures/Background (Nebula Red + Stars) - https://dinvstudio.itch.io/dynamic-space-background-lite-free
EffectExamples/* (Fire Effect) - https://assetstore.unity.com/packages/essentials/tutorial-projects/unity-particle-pack-127325
Mobile Device Touch Icons - https://ewebdesign.com/free-touch-interface-icon/

Sounds:
	Mothership - Explosion: https://assetstore.unity.com/packages/audio/sound-fx/foley/fantasy-sfx-for-particle-distort-texture-effect-library-42146
				 Explosion Buildup - https://assetstore.unity.com/packages/audio/sound-fx/epic-arsenal-essential-elements-demo-packs-38428
	Ship Collision - https://freesound.org/people/PauliusI/sounds/346300/
	Ball - Wall Bounce - https://assetstore.unity.com/packages/audio/sound-fx/free-casual-game-sfx-pack-54116
	Main Menu background - https://soundimage.org/looping-music/ - Game-Menu_v001
	Ball Shooting and charging sounds - https://www.freesfx.co.uk/